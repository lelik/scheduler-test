package com.atlassian.plugins.bitbucket.server;

import com.atlassian.bitbucket.rest.util.ResponseFactory;
import com.atlassian.bitbucket.rest.util.RestUtils;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("schedule")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@Singleton
public class SchedulerTestResource {

    private final SchedulerTestService schedulerTestService;

    SchedulerTestResource(SchedulerTestService schedulerTestService) {
        this.schedulerTestService = schedulerTestService;
    }

    @GET
    public Response schedule() {
        schedulerTestService.schedule();
        return ResponseFactory.ok().build();
    }
}
