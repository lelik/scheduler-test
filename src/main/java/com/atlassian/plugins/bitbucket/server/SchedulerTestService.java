package com.atlassian.plugins.bitbucket.server;

import com.atlassian.scheduler.*;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.status.JobDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulerTestService {

    private static final Logger log = LoggerFactory.getLogger(SchedulerTestService.class);
    private static final JobRunnerKey KEY = JobRunnerKey.of("incrementing-runner");

    private final SchedulerService schedulerService;

    private int counter = 0;

    SchedulerTestService(SchedulerService schedulerService) {
        this.schedulerService = schedulerService;
    }

    public void schedule() {
        JobRunner runner = new IncrementingJobRunner();
        schedulerService.registerJobRunner(KEY, runner);
        for (int i=0; i < 100; i++) {
            JobId id = JobId.of("increment-job-" + i);
            try {
                schedulerService.scheduleJob(id,
                        JobConfig.forJobRunnerKey(KEY)
                                .withRunMode(RunMode.RUN_LOCALLY));
            } catch (SchedulerServiceException e) {
                log.error("Failed to schedule job.", e);
            }

            JobDetails details = schedulerService.getJobDetails(id);
            log.warn("The job details are: {}", details); // These could be `null` if the job has already run
        }
    }

    private class IncrementingJobRunner implements JobRunner {

        @Override
        public JobRunnerResponse runJob(JobRunnerRequest request) {
            log.warn("Running job: '{}'", request.getJobId());
            try {
                Thread.sleep(counter * 1000);
            } catch (InterruptedException e) {
                log.error("Job was interrupted");
                return JobRunnerResponse.failed(e);
            }
            return JobRunnerResponse.success("Counter is: " + ++counter);
        }
    }
}
